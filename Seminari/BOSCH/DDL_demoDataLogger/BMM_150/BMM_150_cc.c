/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file
 *
 * Demo application of printing BMM150 Magnetometer data on serialport(USB virtual comport)
 * every one second, initiated by autoreloaded timer(freertos)
 * 
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XdkSensorHandle.h"
#include "XDK_Datalogger_ih.h"
#include "XDK_Datalogger_ch.h"
#include "BMM_150_ih.h"
#include "BMM_150_ch.h"

/* system header files */
#include <stdio.h>
#include <BCDS_Basics.h>

/* additional interface header files */
#include "BCDS_BSP_LED.h"
#include "BSP_BoardType.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Magnetometer.h"
#include "BCDS_Retcode.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
Magnetometer_XyzData_T getMagDataRaw = { INT32_C(0), INT32_C(0), INT32_C(0), UINT32_C(0) };
Magnetometer_XyzData_T getMagDataUnit = { INT32_C(0), INT32_C(0), INT32_C(0), UINT32_C(0) };
Magnetometer_DataRate_T bmm150dr = MAGNETOMETER_DATARATE_OUT_OF_RANGE;
/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**@brief The function to gets the data BMM150 Magnetometer
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */

void bmm150_getSensorValues(xTimerHandle pxTimer)
{

    /* Return value for Magnetometer Sensor */
    Retcode_T sensorApiRetValue = (Retcode_T) RETCODE_FAILURE;
    (void) pxTimer;

    sensorApiRetValue = Magnetometer_readXyzLsbData(xdkMagnetometer_BMM150_Handle, &getMagDataRaw);
    if (RETCODE_OK == sensorApiRetValue)
    {
        ;
    }
    else
    {
        printf("Magnetometer XYZ lsb Data read FAILED\n\r");
    }

    sensorApiRetValue = Magnetometer_readXyzTeslaData(xdkMagnetometer_BMM150_Handle, &getMagDataUnit);
    if (RETCODE_OK == sensorApiRetValue)
    {
        ;
    }
    else
    {
        printf("Magnetometer XYZ MicroTeslaData read FAILED\n\r");
    }
}

/* global functions ********************************************************* */

/**
 * @brief The function initializes BMM150(magnetometer) and set the sensor parameter from logger.ini
 */
extern void bmm_150_init(void)
{
    Retcode_T returnVal = RETCODE_OK;

    /* Return value for magnetometerInit and magnetometerSetMode api*/
    Retcode_T magReturnValue = (Retcode_T) RETCODE_FAILURE;
    /* Initialization for Magnetometer Sensor */
    magReturnValue = Magnetometer_init(xdkMagnetometer_BMM150_Handle);
    if (RETCODE_OK == magReturnValue)
    {
        printf("BMM150 initialization Success\n\r");

    }
    else
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
             if (RETCODE_OK != returnVal)
             {
                 printf("Turning on of RED LED failed");
             }
        printf("Magnetometer initialization FAILED\n\r");
    }
    if (strcmp(bmm150_data, "10"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_10HZ;
    }
    else if (strcmp(bmm150_data, "2"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_2HZ;
    }
    else if (strcmp(bmm150_data, "6"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_6HZ;
    }
    else if (strcmp(bmm150_data, "15"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_15HZ;
    }
    else if (strcmp(bmm150_data, "20"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_20HZ;
    }
    else if (strcmp(bmm150_data, "25"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_25HZ;
    }
    else if (strcmp(bmm150_data, "8"))
    {
        bmm150dr = MAGNETOMETER_BMM150_DATARATE_8HZ;
    }
    magReturnValue = Magnetometer_setDataRate(xdkMagnetometer_BMM150_Handle, bmm150dr);
    if ((RETCODE_OK != magReturnValue)
            || (MAGNETOMETER_DATARATE_OUT_OF_RANGE == bmm150dr))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
             if (RETCODE_OK != returnVal)
             {
                 printf("Turning on of RED LED failed");
             }
    }
}

/**
 *  @brief API to Deinitialize the PMD module
 */
extern void bmm_150_deInit(void)
{
    Retcode_T returnValue = RETCODE_FAILURE;
    returnValue = Magnetometer_deInit(xdkMagnetometer_BMM150_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("Magnetometer Deinit Success\n\r");
    }
    else
    {
        printf("Magnetometer Deinit Failed\n\r");
    }
}

/*************************************************************************** */
