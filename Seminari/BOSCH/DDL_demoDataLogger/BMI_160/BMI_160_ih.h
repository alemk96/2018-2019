/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Interface header for the BMI_160 module.
 *
 * The interface header exports the following features:  bmi_160_init,
 *                                                       bmi160_deInit
 *
 * ****************************************************************************/
/**
 * @defgroup pid PID
 * @ingroup APP
 *
 * @{
 * @brief  Printing Interial Data using USB printf periodically,triggered by a timer(free Rtos)
 *  \tableofcontents
 *  \section intro_sec PID
 * Demo application of printing BMI160 Accel and Gyro data on serial port(USB virtual com port)
 * every one second, initiated by auto reloaded timer(freeRTOS)
 */

/* header definition ******************************************************** */
#ifndef BMI_160_IH_H_
#define BMI_160_IH_H_

#include "BCDS_Accelerometer.h"
#include "BCDS_Gyroscope.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public function prototype declarations */

/**
 * @brief The function initializes BMI(Interial-accel & gyro)creates and starts a auto reloaded
 * timer task which gets and prints the accel and gyro data.
 *
 */
extern void bmi_160_init(void);

/**
 *  @brief API to deinitialize the PID module
 */
extern void bmi160_deInit(void);

/* public global variable declarations */
extern Accelerometer_XyzData_T getAccelDataRaw160;
extern Accelerometer_XyzData_T getAccelDataUnit160;
extern Gyroscope_XyzData_T getGyroDataRaw160;
extern Gyroscope_XyzData_T getGyroDataConv160;
/* inline function definitions */

#endif /* BMI_160_IH_H_ */

/** ************************************************************************* */
