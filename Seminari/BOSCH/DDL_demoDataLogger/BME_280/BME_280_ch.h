/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Configuration header for the BME_280 file.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BME_280_CH_H_
#define BME_280_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define PED_THREESECONDDELAY  UINT32_C(3000)	 	/** one second is represented by this macro */
#define PED_TIMERBLOCKTIME  UINT32_C(0xffff) 	/** Macro used to define blocktime of a timer*/
#define PED_USEI2CPROTOCOL  UINT8_C(0)	 		/** I2c protocol is represented by this macro*/
#define PED_VALUE_ZERO 		UINT8_C(0)	 		/** default value*/
#define PED_I2C_ADDRESS     UINT8_C(0x18) 	    /** i2c address for BME280 */

/* local function prototype declarations */

/**
 * @brief Gets the raw data from BME Environmental and prints through the USB printf on serial port
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void bme280_getSensorValues(void *pvParameters);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BME_280_CH_H_ */

/** ************************************************************************* */
