/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 * Demo application of printing BME280 Environmental data on serialport(USB virtual comport)
 * every one second, initiated by Environmental timer(freertos)
 * 
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XdkSensorHandle.h"
#include "XDK_Datalogger_ih.h"
#include "XDK_Datalogger_ch.h"
#include "BME_280_ih.h"
#include "BME_280_ch.h"

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* additional interface header files */
#include "BSP_BoardType.h"
#include "BCDS_BSP_LED.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Environmental.h"
#include "BCDS_Retcode.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
/* global variables ********************************************************* */
Environmental_Data_T bme280s = { INT32_C(0), UINT32_C(0), UINT32_C(0) };
Environmental_LsbData_T bme280lsb = { INT32_C(0), INT32_C(0), INT32_C(0) };
Environmental_OverSampling_T bme280os = ENVIRONMENTAL_OVERSAMP_OUT_OF_RANGE;
Environmental_FilterCoefficient_T bme280coeff =
        ENVIRONMENTAL_FILTER_COEFF_OUT_OF_RANGE;
/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/**
 * @brief The function initializes BME(Environmental) and set the sensor parameter from logger.ini
 */
extern void bme_280_init(void)
{
    /* Return value for Timer start */
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnVal = RETCODE_OK;

    /*initialize Environmental sensor*/
    returnValue = Environmental_init(xdkEnvironmental_BME280_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("Environmental Sensor initialization Success\n\r");
    }
    else
    {
        printf("Environmental Sensor initialization Failed\n\r");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }
    }
    /* check the Oversampling value from INi-File*/
    if (strcmp(bme280_os, "1") == 0)
    {
        bme280os = ENVIRONMENTAL_BME280_OVERSAMP_1X;
    }
    else if (strcmp(bme280_os, "2") == 0)
    {
        bme280os = ENVIRONMENTAL_BME280_OVERSAMP_2X;
    }
    else if (strcmp(bme280_os, "4") == 0)
    {
        bme280os = ENVIRONMENTAL_BME280_OVERSAMP_4X;
    }
    else if (strcmp(bme280_os, "8") == 0)
    {
        bme280os = ENVIRONMENTAL_BME280_OVERSAMP_8X;
    }
    else if (strcmp(bme280_os, "16") == 0)
    {
        bme280os = ENVIRONMENTAL_BME280_OVERSAMP_16X;
    }
    else if (strcmp(bme280_os, "off") == 0)
    {
        bme280os = ENVIRONMENTAL_BME280_OVERSAMP_SKIPPED;
    }
    returnValue = Environmental_setOverSamplingHumidity(
            xdkEnvironmental_BME280_Handle, bme280os);
    if ((RETCODE_OK != returnValue)
            || (ENVIRONMENTAL_OVERSAMP_OUT_OF_RANGE == bme280os))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }
    }

    returnValue = Environmental_setOverSamplingPressure(
            xdkEnvironmental_BME280_Handle, bme280os);
    if ((RETCODE_OK != returnValue)
            || (ENVIRONMENTAL_OVERSAMP_OUT_OF_RANGE == bme280os))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }
    }

    returnValue = Environmental_setOverSamplingTemperature(
            xdkEnvironmental_BME280_Handle, bme280os);
    if ((RETCODE_OK != returnValue)
            || (ENVIRONMENTAL_OVERSAMP_OUT_OF_RANGE == bme280os))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }
    }
    /* check the filterCoefficient value from INi-File*/
    if (strcmp(bme280_coeff, "off") == 0)
    {
        bme280coeff = ENVIRONMENTAL_BME280_FILTER_COEFF_OFF;
    }
    else if (strcmp(bme280_coeff, "2") == 0)
    {
        bme280coeff = ENVIRONMENTAL_BME280_FILTER_COEFF_2;
    }
    else if (strcmp(bme280_coeff, "4") == 0)
    {
        bme280coeff = ENVIRONMENTAL_BME280_FILTER_COEFF_4;
    }
    else if (strcmp(bme280_coeff, "8") == 0)
    {
        bme280coeff = ENVIRONMENTAL_BME280_FILTER_COEFF_8;
    }
    else if (strcmp(bme280_coeff, "16") == 0)
    {
        bme280coeff = ENVIRONMENTAL_BME280_FILTER_COEFF_16;
    }
    returnValue = Environmental_setFilterCoefficient(
            xdkEnvironmental_BME280_Handle, bme280coeff);
    if ((RETCODE_OK != returnValue)
            || (ENVIRONMENTAL_FILTER_COEFF_OUT_OF_RANGE == bme280coeff))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }
    }
}

/** The function to get the Environmental data
 * @brief Gets the data from BME280 Environmental
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void bme280_getSensorValues(void *pvParameters)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    (void) pvParameters;
    returnValue = Environmental_readDataLSB(xdkEnvironmental_BME280_Handle,
            &bme280lsb);
    if (RETCODE_OK != returnValue)
    {
        printf("environmentalReadDataLSB FAILED\n\r");
    }
    returnValue = Environmental_readData(xdkEnvironmental_BME280_Handle,
            &bme280s);
    if (RETCODE_OK != returnValue)
    {
        printf("environmentalReadData FAILED\n\r");
    }
}

/**
 *  @brief API to Deinitialize the PED module
 */
extern void bme_280_deInit(void)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    returnValue = Environmental_deInit(xdkEnvironmental_BME280_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("Environmental sensor Deinit Success\n\r");
    }
    else
    {
        printf("Environmental sensor Deinit Failed\n\r");
    }
}

/** ************************************************************************* */
