/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file       XDK_Datalogger_ch.h
 *
 *  Configuration header XDK_Datalogger_cc.c
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef XDK_DATALOGGER_CH_H_
#define XDK_DATALOGGER_CH_H_

/* local interface declaration ********************************************** */

#include "ff.h"
/* local type and macro definitions */
#define SDC_WRITEREAD_DELAY 			UINT32_C(5000) 	 /**< Millisecond delay for WriteRead timer task */
#define SDC_WRITEREAD_BLOCK_TIME 		UINT32_C(0xffff) /**< Macro used to define block time of a timer*/
#define SDC_DETECT_SD_CARD_INSERTED 	UINT8_C(1)
#define SDC_SINGLE_BLOCK				UINT8_C(1)      /**< SD- Card Single block write or read */
#define SDC_DRIVE_ZERO				    UINT8_C(0)      /**< SD Card Drive 0 location */
#define SDC_PARTITION_RULE_FDISK	    UINT8_C(0)      /**< SD Card Drive partition rule 0: FDISK, 1: SFD */
#define SDC_AUTO_CLUSTER_SIZE		    UINT8_C(0)      /**< zero is given, the cluster size is determined depends on the volume size. */
#define SDC_SEEK_FIRST_LOCATION		    UINT8_C(0)      /**< File seek to the first location */

#define MAX_FILE_NAME_LENGTH              UINT8_C(13)
#define MAX_PATH_LENGTH                   UINT8_C(256)

/** structure required to collect Sensor parameter from INI-File on SD-Card */
typedef struct configuration
{
    TCHAR filename[13];
    TCHAR fileformat[7];
    TCHAR dataformat[5];

    uint32_t bma280_enabled;
    uint32_t bma280_sampling_rate_timer_ticks;
    uint32_t bma280_range;
    uint32_t bma280_bandwidth;
    uint32_t bma280_sampling_rate_remaining_ticks;
    uint32_t bma280_sampling_rate;

    uint32_t bmg160_enabled;
    uint32_t bmg160_sampling_rate_timer_ticks;
    uint32_t bmg160_bandwidth;
    uint32_t bmg160_sampling_rate_remaining_ticks;
    uint32_t bmg160_sampling_rate;

    uint32_t bmi160_enabled;
    uint32_t bmi160_sampling_rate_timer_ticks;
    uint32_t bmi160_bandwidth_accel;
    uint32_t bmi160_bandwidth_gyro;
    uint32_t bmi160_range;
    uint32_t bmi160_sampling_rate_remaining_ticks;
    uint32_t bmi160_sampling_rate;

    uint32_t bmm150_enabled;
    uint32_t bmm150_sampling_rate_timer_ticks;
    uint32_t bmm150_data_rate;
    uint32_t bmm150_sampling_rate_remaining_ticks;
    uint32_t bmm150_sampling_rate;

    uint32_t bme280_enabled;
    uint32_t bme280_sampling_rate_timer_ticks;
    uint32_t bme280_oversampling;
    uint32_t bme280_filter_coefficient;
    uint32_t bme280_sampling_rate_remaining_ticks;
    uint32_t bme280_sampling_rate;

    uint32_t max44009_enabled;
    uint32_t max44009_sampling_rate_timer_ticks;
    uint32_t max44009_integration_time;
    uint32_t max44009_sampling_rate_remaining_ticks;
    uint32_t max44009_sampling_rate;
} configuration;
/**/

/* local function prototype declarations */

/** @brief
 * 		Task to Update the enabled Sensors, by calling sampleSensors and write to the ActiveBuffer
 *
 *  @param[in] void *pvParameters
 * 		Rtos task should be defined with the type void *(as argument).
 */
void UpdateSensorValues(void *pvParameters);

/** @brief
 * 		Task to run all Blink interval
 *
 *
 *   @param[in] void *pvParameters
 */
void normal_blink(void *pvParameters);

/** @brief
 * 		The Callback function which is called by the Button pressed-Interrupt handler. Set global Flags, increase Button count and writes new filename to array filename.
 */
void PB0_InterruptCallback(uint32_t parameter);

/** @brief
 * 		The function is used to Scan the Files on SD-Card and get the latest File and its filename.
 *      The current Number is extracted from the file name and is stored in button count variable
 *
 */
FRESULT scan_files(void);

/** @brief
 * 		The function returns the number of lines in the file custlog.ini
 *
 * @return 0 if file doesn't exist or empty, else number of lines in the file
 */
int Count_CustLogLines(void);

/** @brief
 * 		The function reads the first two lines of custlog.ini
 *
 * @param[out] header, string
 * @return 0 if ok, 1 if the file custlog.ini could not be opened
 */
int customLog_LineRead(TCHAR header[], TCHAR string[]);

/** @brief
 * 		The function used to get the Sensor config from logger.ini, and stores the values in the struct configuration
 * 		Processing the ini-file by ini-parser minIni
 *
 * @param[out] TCHAR header[], TCHAR string[]
 */
int getIniValues(void);

/* globale variables Store specific Sensor parameters. Set on Init of the Sensor-API */
extern configuration config;
extern TCHAR bma280_bw[12];
extern TCHAR bmi160_accel_bw[12];
extern TCHAR bmi160_gyro_bw[12];
extern TCHAR bmg160_bw[12];
extern TCHAR bmm150_data[12];
extern TCHAR bme280_os[4];
extern TCHAR bme280_coeff[4];
extern TCHAR MAX44009_int[4];

#endif /* XDK_DATALOGGER_CH_H_ */

/** ************************************************************************* */
