/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Configuration header for the MAX_44009 module.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */

#ifndef MAX_44009_CH_H_
#define MAX_44009_CH_H_

/* local interface declaration ********************************************** */
#include "FreeRTOS.h"
#include "timers.h"

/* local type and macro definitions */
#define LSD_THREESECONDDELAY                UINT32_C(3000)      /**< three second is represented by this macro */
#define LSD_TIMERBLOCKTIME                  UINT32_C(0xffff)    /**< Macro used to define blocktime of a timer */
#define LSD_ZERO                            UINT8_C(0)          /**< default value */
#define LSD_ONE                             UINT8_C(1)          /**< default value */
#define LSD_DEFERRED_CALLBACK               UINT8_C(1)          /**< indicate deferred callback */
#define LSD_REALTIME_CALLBACK               UINT8_C(0)          /**< indicate real time callback */
#define LSD_UPPER_THRESHOLD_VALUE           UINT32_C(0x5a)      /**< upper threshold value */
#define LSD_LOWER_THRESHOLD_VALUE           UINT32_C(0x2a)      /**< lower threshold value */
#define LSD_THRESHOLD_TIMER_VALUE           UINT32_C(0X05)      /**< threshold timer value */
#define LSD_NIBBLE_SIZE                     UINT8_C(4)          /**< size of nibble */
#define LSD_MASK_NIBBLE                     UINT8_C(0x0F)       /**< macro to mask nibble */
#define LSD_EVENNUMBER_IDENTIFIER           UINT8_C(2)          /**< macro to identify even numbers */
#define LSD_APP_CALLBACK_DATA               UINT32_C(100)       /**< macro to indicate application time callback data for demo*/
#define LSD_FAILURE                         INT8_C(-1)          /**< macro to failure state */

/* local function prototype declarations */
/**
 * @brief Read data from light sensor and print through the USB
 *
 * @param[in] pxTimer timer handle
 */
void max44009_getSensorValues(xTimerHandle pxTimer);

/* local module global variable declarations */

/* local inline function definitions */
#endif /* MAX_44009_CH_H_ */

/** ************************************************************************* */
