/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Interface header for the BMA_280.
 *
 * The interface header exports the following features:  bma_280_init,
 *                                                       bma_280_deInit
 *
 * ****************************************************************************/
/**
 * @defgroup pad PAD
 * @ingroup APP
 *
 * @{
 * @brief  Printing Accel Data using USB printf periodically,triggered by a timer(free Rtos)
 *  \tableofcontents
 *  \section intro_sec PAD
 */
/* header definition ******************************************************** */
#ifndef BMA_280_IH_H_
#define BMA_280_IH_H_

#include "BCDS_Accelerometer.h"
/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public function prototype declarations */

/**
 * @brief The function initializes BMA(accelerometer)creates and starts a autoreloaded
 * timer task which gets and prints the accel raw data
 *
 */
extern void bma_280_init(void);

/**
 *  @brief API to de-initialize the PAD module
 */
extern void bma_280_deInit(void);

/* public global variable declarations */
extern Accelerometer_XyzData_T getAccelDataRaw;
extern Accelerometer_XyzData_T getAccelDataUnit;
/* inline function definitions */

#endif /* BMA_280_IH_H_ */

/**@}*/
/** ************************************************************************* */
