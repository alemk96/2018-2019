/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Interface header for the BMG_160 module.
 *
 * The interface header exports the following features:  bmg_160_init,
 *                                                       bmg_160_deInit
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BMG_160_IH_H_
#define BMG_160_IH_H_

#include "BCDS_Gyroscope.h"
/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public function prototype declarations */

/**
 * @brief The function initializes BMG160 sensor and creates, starts timer task in autoreloaded mode
 * every three second which reads and prints the Gyro sensor data
 */
void bmg_160_init(void);

/**
 *  @brief  the function to deinitialize
 *
 */
void bmg_160_deInit(void);

/* public global variable declarations */
extern Gyroscope_XyzData_T getRawData;
extern Gyroscope_XyzData_T getMdegData;
/* inline function definitions */

#endif /* BMG_160_IH_H_ */

/** ************************************************************************* */
