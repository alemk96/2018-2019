/* Alternative papabili:
- https://github.com/DrGFreeman/TimedPID
- https://r-downing.github.io/AutoPID/
- https://github.com/mike-matera/FastPID (SCELTA)
*/

/**
 * Rotor versione a task con PID
 *
 * ********************************************
 *           BOARD = lolin esp32
 * ********************************************
 */

// ESP32 pins
#define ROTOR 12
#define PHOTO 15
#define REED 13
#define POTENZIOMETRO 14

#include <FastPID.h>
float Kp=0.5, Ki=0.1, Kd=0.5, Hz=10; // atrent: attenzione agli Hz, vanno calcolati in funzione del periodo del task!!!
FastPID pidController(Kp, Ki, Kd, Hz, 16, true);
int pidResult=0;
#define USEPID

// costanti parametri config
#define SAMPLES 300
#define DUTY_PERIOD 500
#define MAXRPM 500
#define MAX_DUTY_PERC 80	// per non esagerare
#define MIN_DUTY_PERC 5	// per non fermarlo mai (che poi potrebbe non ripartire)
#define DEBOUNCE_MU 8000	// da tarare
#define MAXANALOG 4096

// variabili di stato (globali, ragionare su uso memoria)
volatile long lastTick=0;
volatile long lastPulse=0;
volatile long period=0;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

//int desiredRpm=100; // molto instabile
//int desiredRpm=200; // instabile
int desiredRpm=300; // quasi stabile
//int desiredRpm=400; // più o meno stabile
//int desiredRpm=800; // chi lo sa... (se ci arriva)

int incremento=50; // per veder variare l'instabilità di inseguimento del desired in funzione del valore dell'incremento (analogo a variare il Kp in una funzione PID senza I e D)

int photovalues[SAMPLES];
int cursor=0;

int potenziometro=0;

int absDuty=150; // default (check HIGH/LOW!)

/** restituisce il numero di giri al minuto, partendo dal periodo in millisec
 */
int rpm() {
    // va bene anche divisione intera, ma attenzione al divByZero!!!
    if(period!=0)
        return 60000000/period;
    return 0;
}

#include <TaskScheduler.h>
Scheduler runner;

/** plot su oled */
Task taskOled(10*TASK_MILLISECOND, TASK_FOREVER, statusOled);

/** stampa qualche semplice dato per rappresentazione "plotter" */
Task taskPlotter(100*TASK_MILLISECOND, TASK_FOREVER, plotterCallback);

/** stampa uno stato più "parlante" per debugging */
Task taskStatus(2*TASK_SECOND, TASK_FOREVER, statusCallback);

/** legge potenziometro */
Task taskPotenziometro(100*TASK_MILLISECOND, TASK_FOREVER, []() {
    potenziometro=analogRead(POTENZIOMETRO);
    desiredRpm=map(potenziometro,0,MAXANALOG,0,MAXRPM);
});

/** invoca PID e calcola "output" */
Task taskPID(100*TASK_MILLISECOND, TASK_FOREVER, []() {
    pidResult=map(
                  pidController.step(desiredRpm, rpm()), // calcolo funzione di trasferimento
                  0,MAXRPM,0,100);
});

/** campiona la fotoresistenza (per uso futuro) */
Task taskPhotocellRead(TASK_MILLISECOND, TASK_FOREVER, []() {
    photovalues[cursor] = analogRead(PHOTO);
    cursor++;
    if(cursor>=SAMPLES) cursor=0;
});

/** calcola duty in funzione della velocità desiderata, fare varie prove */
Task taskControl(50*TASK_MILLISECOND, TASK_FOREVER, []() {
#ifdef USEPID
    setDutyPercentage(pidResult); // TODO verificare scala!!!
#else
    // esempio banale (inseguitore a incremento fisso)
    if(desiredRpm-rpm()>0)
        setDutyPercentage(getDutyPercentage()+incremento);
    else if(desiredRpm-rpm()<0)
        setDutyPercentage(getDutyPercentage()-incremento);
    else if(desiredRpm==0)
        setDutyPercentage(0);
#endif
});

/** legge input da seriale (interfaccia utente) e cambia i parametri di lavoro (velocità desiderata) */
Task taskSerial(2*TASK_SECOND, TASK_FOREVER, []() {
    if(Serial.available()>0) {
        //setDutyPercentage(Serial.readStringUntil('\n').toInt()); // NON pilotare direttamente duty, impostare un rpm desiderato e lasciare che sia il "controllo" a calcolare duty
        desiredRpm=Serial.readStringUntil('\n').toInt(); // attenzione ai timeout!
        Serial.println("...data received");
    }
});

/** controlla (accende/spegne) attivazione motore in base ai parametri */
Task taskMotor(10*TASK_MILLISECOND, TASK_FOREVER, []() {
    // ragionare sul <> in funzione del HIGH/LOW della board (a volte ribaltato)
    if(millis()%DUTY_PERIOD < absDuty) // così non è in percentuale, ma va rapportato a DUTY_PERIOD (cmq basta impostare absDuty via funzione "setter")
        //if(millis()%DUTY_PERIOD < 150) // versione fissa per testing
        digitalWrite(ROTOR, HIGH);
    else
        digitalWrite(ROTOR, LOW);
});

/** ISR  */
void reedRead() {
    noInterrupts(); // ESP8266, Arduino
    portENTER_CRITICAL(&mux); // ESP32

    long tick=micros();
    long tmpPeriod=tick-lastTick; // da validare, se dura più di DEBOUNCE

    // DONE nonostante tutto molti rimbalzi, introdurre "debounce"
    if(tmpPeriod>DEBOUNCE_MU) {
        period=2*tmpPeriod; // ci sono DUE magneti! altrimenti sarebbe un emiperiodo
        lastTick=tick;
        //Serial.print("*"); // ATTENZIONE! bruttissimo chiamare "cose complesse" da dentro una ISR!
    } //else
    //Serial.print("."); // ATTENZIONE! bruttissimo chiamare "cose complesse" da dentro una ISR!

    // provare pulsein? https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/
    //lastPulse=pulseIn(REED,HIGH); // HIGH perché l'interrupt è sul FALLING
    // vedere quanto blocca questa invocazione... ;)

    portEXIT_CRITICAL(&mux); // ESP32
    interrupts(); // ESP8266, Arduino
}

/** getter per la percentuale */
int getDutyPercentage() {
    return map(absDuty,0,DUTY_PERIOD,0,100);
}

/** setter per la percentuale */
void setDutyPercentage(int perc) {
    /*
    Serial.print("# % richiesta:");
    Serial.println(perc);
    */

    if(perc>MAX_DUTY_PERC)
        perc=MAX_DUTY_PERC;

    if(perc<0)
        perc=0;

    if(desiredRpm>0 && perc==0)
        perc=MIN_DUTY_PERC;

    absDuty = map(perc,0,100,0,DUTY_PERIOD);
}

///////////////////////////////////////////////////////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println("< booting...");

    pinMode(ROTOR, OUTPUT);
    pinMode(REED, INPUT_PULLUP); // quindi il reed va messo fra pin e massa (sarà "attivo basso")

    // https://techtutorialsx.com/2017/09/30/esp32-arduino-external-interrupts/
    // e anche esempio GPIOinterrupt
    // timer interrupt: https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/

    attachInterrupt(digitalPinToInterrupt(REED), reedRead, FALLING);

    oled_setup();

    // tasks
    runner.init();

    runner.addTask(taskPlotter);
    runner.addTask(taskStatus);
    runner.addTask(taskPhotocellRead);
    runner.addTask(taskSerial);
    runner.addTask(taskMotor);
    runner.addTask(taskControl);
    runner.addTask(taskOled);
    runner.addTask(taskPID);
    runner.addTask(taskPotenziometro);

    taskPlotter.enable();
    //taskStatus.enable();
    //taskPhotocellRead.enable();
    //taskSerial.enable();
    taskMotor.enable();
    taskControl.enable();
    taskOled.enable();
    taskPID.enable();
    taskPotenziometro.enable();

    Serial.println("setup complete! >");
}

////////////////////////////////////////////////////////
void loop() {
    runner.execute(); // TaskScheduler
}
////////////////////////////////////////////////////////
