# Diario lezioni

Nota bene: a lezione non è detto si riesca ad affrontare tutti gli argomenti, ma per l'esame vanno conosciuti i capitoli del libro elencati nel README.

## Lezioni (in ordine cronologico inverso)

### Future

Le date "future" cambiano (riprogrammazione in funzione di eventi vari) man mano che andiamo avanti col corso.

* 2019-06-14, discussione idee progetti, *a seguire (nel pomeriggio) esame* (se ci sono candidati), FINE CORSO!!!

### Passate
* 2019-06-10, verifica charlieplexing, CS (Chip Select), motori (ponte H), multiplexing, shift register
* 2019-06-07, workshop saldatura (realizziamo esempio a 6 LED da https://en.wikipedia.org/wiki/Charlieplexing)
* 2019-06-03, protocolli comunicazione "wired" (seriale, i2c, firmata, bit banging), attuatori (knob)
* 2019-05-31, ambiente Raspberry+ArduinoIDE (studenti)
* (2019-05-27), SOSPENSIONE lezioni (post elettorale)
* 2019-05-24, (Alexjan Carraturo) "Embedded Systems"
* 2019-05-20, protocolli comunicazione "rete" (es. mqtt, osc)
* (2019-05-17), NON c'è lezione
* 2019-05-13, controlli automatici, PID (esercizio con lib)
* 2019-05-10, Intervento di ST
* 2019-05-06, RotorTasker (con TaskScheduler) e intro esigenza controlli automatici
* 2019-05-03, TaskScheduler con esercizio
* (2019-04-29), NON c'è lezione
* (2019-04-26), vacanza accademica
* (2019-04-22), vacanza accademica
* (2019-04-19), vacanza accademica
* 2019-04-15, interrupt, motivando per confronto con sensore photoresist o photocell e reed (polled)
* 2019-04-12, (lab) completamento esercizio rgb, inizio esercizio polling digitalpin, cenni uso lib
* 2019-04-08, Intervento di BOSCH
* 2019-04-05, (lab) Rotor (esempio PWM dei poveri, lettura analogica, lettura pullup, reed), PWM vera con LED (eventualmente RGB), dato esercizio RGB+potenziometro
* 2019-04-01, forme d'onda, PWM, analogWrite
* 2019-03-29, (lab) I/O input (anche PULLUP - da sperimentare ancora) + analogici
* 2019-03-25, fine componenti (compreso transistor e c.i.), strumenti di misura, cenni sui sensori
* 2019-03-22, (lab) I/O inizio, collegamento componentistica
* 2019-03-18, componenti, specie condensatori, RC
* 2019-03-15, (lab) Arduino IDE, intro linguaggio
* 2019-03-11, richiamo sui principi, leggi di Ohm e Kirchhoff, resistenze, partitore, serie e parallelo di resistenze, interruttori, lampadine, relé
* 2019-03-04, intro Sistemi Embedded, architetture, monoprog e multiprog, MdT, memoria
* 2019-03-01, intro corso, panoramica Sistemi Embedded


## Argomenti da evadere (non in ordine di presentazione, non riusciremo a trattare tutti i topic in aula)

FIXME cfr. file lista domande
